package com.bernhard_posselt.helloworld.api;

import java.util.List;

/**
 * Created by ray on 12/2/14.
 */
public class FeedPackage {
    public List<Feed> feeds;
    public int starredCount;
    public int newestItemId;
}
