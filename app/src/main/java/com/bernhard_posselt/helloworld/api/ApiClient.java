package com.bernhard_posselt.helloworld.api;

import android.util.Base64;

import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by ray on 12/2/14.
 */
public class ApiClient {

    private final RestAdapter builder;
    private final FeedApi feedApi;
    private String baseUrl;
    private String user;
    private String password;

    public ApiClient(String baseUrl, final String user, final String password) {
        this.baseUrl = baseUrl;
        this.user = user;
        this.password = password;

        RequestInterceptor interceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                String auth = user + ":" + password;
                auth = Base64.encodeToString(auth.getBytes(), Base64.DEFAULT);
                request.addHeader("Authorization", "Basic " + auth);
            }
        };

        builder = new RestAdapter.Builder()
                .setEndpoint(baseUrl)
                .setRequestInterceptor(interceptor)
                .build();

        feedApi = builder.create(FeedApi.class);
    }


    public void getFeeds(Callback<FeedPackage> callback) {
        feedApi.listFeeds(callback);
    };

}
