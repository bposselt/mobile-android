package com.bernhard_posselt.helloworld.api;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by ray on 12/2/14.
 */
public interface FeedApi {
    @GET("/feeds")
    void listFeeds(Callback<FeedPackage> callback);
}
