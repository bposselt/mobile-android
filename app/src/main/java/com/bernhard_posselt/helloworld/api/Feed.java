package com.bernhard_posselt.helloworld.api;

/**
 * Created by ray on 12/2/14.
 */

public class Feed {
    public int id;
    public String url;
    public String title;
    public String faviconLink;
    public int added;
    public int folderId;
    public int unreadCount;
    public String link;
}
