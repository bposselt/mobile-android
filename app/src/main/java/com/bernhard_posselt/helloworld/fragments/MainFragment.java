package com.bernhard_posselt.helloworld.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bernhard_posselt.helloworld.R;
import com.bernhard_posselt.helloworld.api.ApiClient;
import com.bernhard_posselt.helloworld.api.Feed;
import com.bernhard_posselt.helloworld.api.FeedPackage;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ray on 11/25/14.
 */
public class MainFragment extends Fragment {

    @InjectView(R.id.get_feeds)
    Button getFeeds;

    @InjectView(R.id.user)
    EditText user;

    @InjectView(R.id.password)
    EditText password;

    @InjectView(R.id.list)
    ListView list;

    @InjectView(R.id.progress)
    ProgressBar progressbar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, rootView);
        progressbar.setVisibility(View.INVISIBLE);
        final Context ctx = getActivity().getApplicationContext();


        final ArrayList<String> messages = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                ctx,
                R.layout.feed_item,
                messages
        );
        list.setAdapter(adapter);


        getFeeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbar.setVisibility(View.VISIBLE);
                progressbar.setProgress(0);
                progressbar.setIndeterminate(true);
                final String apiUser = user.getText().toString();
                final String apiPassword = password.getText().toString();

                ApiClient client = new ApiClient("https://cloud.w1r3.org/index.php/apps/news/api/v1-2",
                        apiUser, apiPassword);
                messages.clear();
                client.getFeeds(new Callback<FeedPackage>() {
                    @Override
                    public void success(FeedPackage feedPackage, Response response) {
                        List<Feed> feeds = feedPackage.feeds;
                        for(Feed feed: feeds) {
                            messages.add(feed.title);
                        }
                        adapter.notifyDataSetChanged();
                        progressbar.setProgress(100);
                        progressbar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        // haha, no way to get the http error code, what a bad rest lib :/
                        if (error.getMessage().startsWith("401")) {
                            PermissionDeniedDialog fragment = new PermissionDeniedDialog();
                            fragment.show(getFragmentManager(), "Unauthorized");
                        } else {
                            String msg = error.getMessage() + " yo " + error.getKind();
                            Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                        progressbar.setProgress(100);
                        progressbar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });



        return rootView;
    }


}